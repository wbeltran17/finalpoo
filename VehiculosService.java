import java.util.ArrayList;

public interface VehiculosService {

    public Vehiculos crearVehiculos(ArrayList centrosDistribucion);

    public Vehiculos venderVehiculos(ArrayList vehiculos);

    public ArrayList listarVehiculos();
}