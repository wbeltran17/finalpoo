
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class CentroDistribucionImpl implements CentroDistribucionServicios{
    
    @Override
    public CentroDistribucion crearCentroDistribucion(){

        String nombre = JOptionPane.showInputDialog("Ingrese el nombre del nuevo centro de Distribucion");

        CentroDistribucion centroDistribucion = new CentroDistribucion();
        centroDistribucion.setNombre(nombre);

        return centroDistribucion;
    }

    @Override
    public void ventasPorCentro(ArrayList centrosDistribucion, ArrayList ventas){

        int opc = 0;
        String opcString = "";
        for (int i = 0; i < centrosDistribucion.size(); i++) {
            CentroDistribucion centroDistribucion = (CentroDistribucion) centrosDistribucion.get(i);
            opcString += i + 1 + ". " + centroDistribucion.getNombre() + " \n";
        }

        opc = Integer.parseInt(
                JOptionPane.showInputDialog("Escoje el Centro de Distribucion al cual consultar:\n" + opcString));

        if (centrosDistribucion.get(opc - 1) != null) {
            CentroDistribucion centroDistribucion = (CentroDistribucion) centrosDistribucion.get(opc - 1);

            int totalVehiculos = 0;
            int totalRecaudo = 0;

            for (int i = 0; i < ventas.size(); i++) {
                Vehiculos vehiculo = (Vehiculos) ventas.get(i);

                if (vehiculo.getCentroDistribucion().equals(centroDistribucion.getNombre())){
                    totalVehiculos ++;
                    totalRecaudo += vehiculo.getPrecio();
                }
            }

            JOptionPane.showMessageDialog(null, "El " + centroDistribucion.getNombre() + "\n"
            + "El total de vehiculos vendidos es: " + totalVehiculos + "\n"
            + "El recauda total es de: " + totalRecaudo , "Volumen de Ventas", 1);
        }

    }

    @Override
    public void ventasPorModelo(ArrayList ventas){

        int opc;
        String modelo = ""; 
        opc = Integer.parseInt(JOptionPane.showInputDialog(
            "Escoje el modelo de vehiculo a crear\n" 
            + "1. Modelo 1 \n"
            + "2. Modelo 2 \n" 
            + "3. Modelo 3 \n" 
            + "4. Modelo 4 \n"));
    
            switch (opc) {
                case 1:
                modelo = "Modelo 1";
                    break;
                case 2:
                modelo = "Modelo 2";
                    break;
                case 3:
                modelo = "Modelo 3";
                    break;
                case 4:
                modelo = "Modelo 4";
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Opcion Invalida", "Opcion Invalida", 1);
            }

            int totalVehiculos = 0;
            int totalRecaudo = 0;

            for (int i = 0; i < ventas.size(); i++) {
                Vehiculos vehiculo = (Vehiculos) ventas.get(i);

                if (vehiculo.getModelo().equals(modelo)){
                    totalVehiculos ++;
                    totalRecaudo += vehiculo.getPrecio();
                }
            }

            JOptionPane.showMessageDialog(null, "El " + modelo + "\n"
            + "El total de vehiculos vendidos es: " + totalVehiculos + "\n"
            + "El recauda total es de: " + totalRecaudo , "Volumen de Ventas", 1);
    }
}