import java.util.ArrayList;

public class CentroDistribucion {

    private String nombre;

    private ArrayList vehiculos = new ArrayList<>();

    
    public CentroDistribucion() {
    }

    public CentroDistribucion(String nombre, ArrayList vehiculos) {
        this.nombre = nombre;
        this.vehiculos = vehiculos;
    }

    
    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public String getNombre(){
        return this.nombre;
    }

    public ArrayList getVehiculos() {
        return vehiculos;
    }

    public void setVehiculos(ArrayList vehiculos) {
        this.vehiculos = vehiculos;
    }


}