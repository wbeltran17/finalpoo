
public class Vehiculos{


    private String modelo;

    private int cantidad;

    private int precio;

    private String centroDistribucion;

    public void setModelo(String modelo){
        this.modelo = modelo;
    }

    public String getModelo(){
        return this.modelo;
    }

    public void setCantidad(int cantidad){
        this.cantidad = cantidad;
    }

    public int getCantidad(){
        return this.cantidad;
    }

    public void setPrecio(int precio){
        this.precio = precio;
    }

    public int getPrecio(){
        return this.precio;
    }

    public void setCentroDistribucion(String centroDistribucion){
        this.centroDistribucion = centroDistribucion;
    }

    public String getCentroDistribucion(){
        return this.centroDistribucion;
    }

    @Override
    public String toString(){
        return "Vehiculo # "
        +"\nModelo: " + this.modelo
        +"\nCantidad: " + this.cantidad
        +"\nPrecio: " + this.precio;
    }


}