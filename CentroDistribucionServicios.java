import java.util.ArrayList;

public interface CentroDistribucionServicios {

    public CentroDistribucion crearCentroDistribucion();
    
    public void ventasPorCentro(ArrayList centrosDistribucion, ArrayList ventas);

    public void ventasPorModelo(ArrayList ventas);
}