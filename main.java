import java.util.ArrayList;

import javax.swing.JOptionPane;

public class main {

    private static ArrayList centrosDistribucion = new ArrayList<>();
    private static ArrayList ventas = new ArrayList<>();
    private static VehiculosImpl vehiculosImpl = new VehiculosImpl();
    private static CentroDistribucionImpl centroDistribucionImpl = new CentroDistribucionImpl();

    public static void main(String[] args) {
        cargarCentros();
        int opc = 0;

        do {
            try {
                opc = Integer.parseInt(JOptionPane.showInputDialog("Menú \n\n" 
                        + "1. Agregar automóviles por modelo.\n"
                        + "2. Agregar centros de distribución.\n"
                        + "3. vender automóviles.\n"
                        + "4. Consultar volumen de ventas por centro de distribución.\n"
                        + "5. Unidades vendidas por modelo.\n"
                        + "6. Facturar (cliente, auto, monto venta, centro y fecha).\n"
                        + "7. Porcentaje de unidades de cada modelo vendido en cada centro sobre el total de ventas de la empresa.\n"
                        + "8. Salir"));

                switch (opc) {
                    case 1:
                        vehiculosImpl.crearVehiculos(centrosDistribucion);
                        break;
                    case 2:
                        centrosDistribucion.add(centroDistribucionImpl.crearCentroDistribucion());
                        break;
                    case 3:
                        ventas.add(vehiculosImpl.venderVehiculos(centrosDistribucion));
                        break;
                    case 4:
                        centroDistribucionImpl.ventasPorCentro(centrosDistribucion, ventas);
                        break;
                    case 5:
                        centroDistribucionImpl.ventasPorModelo(ventas);
                        break;
                    case 6:
                        JOptionPane.showMessageDialog(null, "Opcion Invalida", "Opcion Invalida", 1);
                        break;
                    case 7:
                        JOptionPane.showMessageDialog(null, "Opcion Invalida", "Opcion Invalida", 1);
                        break;
                    case 8:
                        JOptionPane.showMessageDialog(null, "Gracias por su visita", "gracias por su visita", 1);
                        break;
                    default:
                        JOptionPane.showMessageDialog(null, "Opcion Invalida", "Opcion Invalida", 1);
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Opcion Invalida", "Opcion Invalida", 1);
            }

        } while (opc != 8);
    }


    public static void cargarCentros(){
        centrosDistribucion.add(new CentroDistribucion("Centro Distribucion 1", new ArrayList<>()));
        centrosDistribucion.add(new CentroDistribucion("Centro Distribucion 2", new ArrayList<>()));
        centrosDistribucion.add(new CentroDistribucion("Centro Distribucion 3", new ArrayList<>()));
        centrosDistribucion.add(new CentroDistribucion("Centro Distribucion 4", new ArrayList<>()));
    }
}