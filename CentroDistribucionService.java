import java.util.ArrayList;

public interface CentroDistribucionService {

    public CentroDistribucion crearCentroDistribucion();
    
    public void ventasPorCentro(ArrayList centrosDistribucion, ArrayList ventas);

    public void ventasPorModelo(ArrayList ventas);
}