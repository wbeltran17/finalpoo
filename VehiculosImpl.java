import java.lang.reflect.Array;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class VehiculosImpl implements VehiculosServicios {

    @Override
    public Vehiculos crearVehiculos(ArrayList centrosDistribucion) {

        int opc;
        Vehiculos vehiculoNuevo = new Vehiculos();

        opc = Integer.parseInt(JOptionPane.showInputDialog(
            "Escoje el modelo de vehiculo a crear\n" 
            + "1. Modelo 1 \n"
            + "2. Modelo 2 \n" 
            + "3. Modelo 3 \n" 
            + "4. Modelo 4 \n"));

        switch (opc) {
            case 1:
                vehiculoNuevo.setModelo("Modelo 1");
                vehiculoNuevo.setPrecio(9000);
                break;
            case 2:
                vehiculoNuevo.setModelo("Modelo 2");
                vehiculoNuevo.setPrecio(10500);
                break;
            case 3:
                vehiculoNuevo.setModelo("Modelo 3");
                vehiculoNuevo.setPrecio(14500);
                break;
            case 4:
                vehiculoNuevo.setModelo("Modelo 4");
                vehiculoNuevo.setPrecio(17200);
                break;
            default:
                JOptionPane.showMessageDialog(null, "Opcion Invalida", "Opcion Invalida", 1);
        }

        String modelos = "";
        for (int i = 0; i < centrosDistribucion.size(); i++) {
            CentroDistribucion centroDistribucion = (CentroDistribucion) centrosDistribucion.get(i);
            modelos += i + 1 + ". " + centroDistribucion.getNombre() + " \n";
        }

        opc = Integer.parseInt(
                JOptionPane.showInputDialog("Escoje el Centro de Distribucion del vehiculo a crear\n" + modelos));

        boolean sw = true;

        if (centrosDistribucion.get(opc - 1) != null) {
            CentroDistribucion centroDistribucion = (CentroDistribucion) centrosDistribucion.get(opc - 1);
            vehiculoNuevo.setCentroDistribucion(centroDistribucion.getNombre());
            ArrayList vehiculos = centroDistribucion.getVehiculos();

            for (int i = 0; i < vehiculos.size(); i++) {
                Vehiculos vehiculo = (Vehiculos) vehiculos.get(i);
                if (vehiculo.getModelo().equals(vehiculoNuevo.getModelo())) {
                    sw = false;
                    vehiculoNuevo.setCantidad(vehiculo.getCantidad() + 1);
                    vehiculos.remove(i);
                    vehiculos.add(i,vehiculoNuevo);
                    break;
                }
            }

            if (sw){
                vehiculoNuevo.setCantidad(1);
                vehiculos.add(vehiculoNuevo);
            }

            centroDistribucion.setVehiculos(vehiculos);
        }

        return vehiculoNuevo;
    }

    @Override
    public Vehiculos venderVehiculos(ArrayList centrosDistribucion) {
        Vehiculos vehiculoVendido =new Vehiculos();
        vehiculoVendido.setCantidad(1);
        int opc;
        String opString = "";
        for (int i = 0; i < centrosDistribucion.size(); i++) {
            CentroDistribucion centroDistribucion = (CentroDistribucion) centrosDistribucion.get(i);
            opString += i + 1 + ". " + centroDistribucion.getNombre() + " \n";
        }

        opc = Integer.parseInt(JOptionPane.showInputDialog("Escoje el Centro de Distribucion\n" + opString));
        opString = "";
        if (centrosDistribucion.get(opc - 1) != null) {
            CentroDistribucion centroDistribucion = (CentroDistribucion) centrosDistribucion.get(opc - 1);
            opString = "Vehiculos disponobles en " + centroDistribucion.getNombre()+ " \n";

            for (int i = 0; i < centroDistribucion.getVehiculos().size(); i++) {
                opString += i + 1 + ". " +  centroDistribucion.getVehiculos().get(i).toString()+ " \n";
            } 
            opc = Integer.parseInt(JOptionPane.showInputDialog("Escoje el Modelo \n" + opString));

            ArrayList vehiculos = centroDistribucion.getVehiculos();

            if (vehiculos.get(opc-1) != null){

                Vehiculos vehiculo = (Vehiculos) vehiculos.get(opc-1);

                vehiculoVendido.setModelo(vehiculo.getModelo());
                vehiculoVendido.setCentroDistribucion(vehiculo.getCentroDistribucion());
                vehiculoVendido.setPrecio(vehiculo.getPrecio());

                if(vehiculo.getCantidad()> 1){

                    vehiculo.setCantidad(vehiculo.getCantidad() -1);

                }else{
                    vehiculos.remove(opc -1);
                }
            }

            centroDistribucion.setVehiculos(vehiculos);
        }

        return vehiculoVendido;
    }

    @Override
    public ArrayList listarVehiculos() {

        return null;
    }

}